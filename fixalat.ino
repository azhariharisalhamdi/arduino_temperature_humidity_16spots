#include <DHT.h>
#include <SPI.h>
#include <SD.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>
#include <Keypad.h>
#include <math.h>
#include <string.h>
#include <DS3231.h>

DS3231  rtc(SDA, SCL);

#define dhtpin1 49
#define dhtpin2 48
#define dhtpin3 47
#define dhtpin4 46
#define dhtpin5 45
#define dhtpin6 44
#define dhtpin7 43
#define dhtpin8 42
#define dhtpin9 41
#define dhtpin10 40
#define dhtpin11 39
#define dhtpin12 38
#define dhtpin13 37
#define dhtpin14 36
#define dhtpin15 35
#define dhtpin16 34

#define DHTTYPE DHT11   // DHT 11

/*
MEGA pin 50 (MISO) to Arduino Ethernet Shield pin 12.
MEGA pin 51 (MOSI) to Arduino Ethernet Shield pin 11.
MEGA pin 52 (SCK) to Arduino Ethernet Shield pin 13.
MEGA pin 53 (SS) to Arduino Ethernet Shield pin 10.
*/

template <class par> int tuliseeprom(int alamat, const par& param)
{
   const byte* p = (const byte*)(const void*)&param;
   int i;
   for (i = 0; i < sizeof(param); i++)
       EEPROM.write(alamat++, *p++);
   return i;
}

template <class par> int bacaeeprom(int alamat, par& param)
{
   byte* p = (byte*)(void*)&param;
   int i;
   for (i = 0; i < sizeof(param); i++)
       *p++ = EEPROM.read(alamat++);
   return i;
}

String waktu, waktu_sebelum, cekkelembapan, cektemperatur, waktuselesaitemperatur, waktuselesaikelembapan;
int n=0, n_sebelum=0;
double xh[2][16], xt[2][16], datakelembapan[2][16], datatemperatur[2][16], hrefdata[2][16], trefdata[2][16];
double h[16], t[16],href=0, tref=0; // float / double / long

const int chipSelect = 4; 
const int up = 25;
const int down = 26; 
const int select = 24;
const int menu = 27;
const int buzzer = 9;

String header, header1, nosensor="", waktuhitung, satuan;

int waktuset=0, ind=0, indmenu=0, indwaktu=0, indkalib=0;
const int statuskalibaddr=0, nsensorkalibaddr=1, koefaddr=2, trefaddr1=3, waktudetik=4, waktumenit=5, waktujam=6;   //mapping memori EEPROM
int stateup=0, statedown=0, stateselect=0, statemenu=0;

float bilref[10];
int var;
double ha0[16], ha1[16], ta0[16], ta1[16], hsigmaxiyi=0, hsigmaxi=0, hsigmayi=0,  tsigmaxiyi=0, tsigmaxi=0, tsigmayi=0,  hsigmaxiquad=0, tsigmaxiquad=0, hrefa0[16], hrefa1[16], trefa0[16], trefa1[16];
double xha0[16], xha1[16], xta1[16], xta0[16], nxha0, nxha1, nxta0, nxta1;
//long double ;
int nrefdata;
int ndatakalib, ndatakalibrasi;
double ehrefa0[16], ehrefa1[16], etrefa0[16], etrefa1[16];
const int jumdata=5;
int angka1, angka;
//int jumlahdata;
int mode;
double ang;
int datake, sensorke, nmaks=0;
//double mantrefa0[16], mantrefa1[16], manhrefa0[16], manhrefa1[16];
int koefstatus, nsensorkalib;

unsigned long waktukerja, bacawaktudetik, bacawaktumenit,bacawaktujam;
String caraambildata;
int nsetwaktu, nsetwaktudetik, nsetwaktumenit, nsetwaktujam, npreview;
int jumlahdata, jumlahdata1, m;
double hrefa1x,hrefa0x, trefa0x, trefa1x;

char desimalmaks[20];
byte presisi=4, lebardesimal=3;
int komm=0, statuskalibrasi;
int neeprom;
int nperiksa;
String statusnsensor[16], statuslogger;
double koefta0[16], koefta1[16], koefha0[16], koefha1[16];
int nkoef;
int modex;
int nedit, nkalib, indmenukalib;
int ndataref;

DHT dht0(dhtpin1, DHTTYPE);
DHT dht1(dhtpin2, DHTTYPE);
DHT dht2(dhtpin3, DHTTYPE);
DHT dht3(dhtpin4, DHTTYPE);
DHT dht4(dhtpin5, DHTTYPE);
DHT dht5(dhtpin6, DHTTYPE);
DHT dht6(dhtpin7, DHTTYPE);
DHT dht7(dhtpin8, DHTTYPE);
DHT dht8(dhtpin9, DHTTYPE);
DHT dht9(dhtpin10, DHTTYPE);
DHT dht10(dhtpin11, DHTTYPE);
DHT dht11(dhtpin12, DHTTYPE);
DHT dht12(dhtpin13, DHTTYPE);
DHT dht13(dhtpin14, DHTTYPE);
DHT dht14(dhtpin15, DHTTYPE);
DHT dht15(dhtpin16, DHTTYPE);

LiquidCrystal lcd(33, 32, 31, 30, 29, 28);

const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'.','0','#'}
};

byte rowPins[ROWS] = {2, 3, 4, 5}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {6, 7, 8}; //connect to the column pinouts of the keypad

Keypad pad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

double manhrefa0[]={
            0,00,           //mulai 1
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00           //mulai 16
};
double manhrefa1[]={
            0,00,           //mulai 1
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00           //mulai 16
};
double mantrefa0[]={
            0,00,           //mulai 1
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00            //mulai 16
};
double mantrefa1[]={
            0,00,           //mulai 1
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00,
            0,00            //mulai 16
};                                  
/*
void tombol();
void tampilawal();
void tampillcd();
void hitungwaktu();
void bunyitombol();
void start();
void setwaktu();
void menukalib();
void setwaktudetik();
void setwaktumenit();
void setwaktujam();
void kekalib();
void refdata_h();
void refdata_t();
void kalibrasi();
void standby();
void ndata();
void selesai();
void modekerja();
void preview();
*/
void setup() {
  pinMode(buzzer, OUTPUT);
  Serial.begin(115200);
  SD.begin(chipSelect);
  rtc.begin();
  dht0.begin();
  dht1.begin();
  dht2.begin();
  dht3.begin();
  dht4.begin();
  dht5.begin();
  dht6.begin();
  dht7.begin();
  dht8.begin();
  dht9.begin();
  dht10.begin();
  dht11.begin();
  dht12.begin();
  dht13.begin();
  dht14.begin();
  dht15.begin();

  lcd.begin(16, 2);

  pinMode(up, INPUT_PULLUP);
  pinMode(down, INPUT_PULLUP);
  pinMode(select, INPUT_PULLUP);
  pinMode(menu, INPUT_PULLUP);
   

for(int k=0; k<2; k++){
  for(int l=0; l<16; l++)
  {
    nosensor+=String(l);
    if(l<16)
    {
      nosensor+=String("\t");
    }
  }
}
  
  
  header=String("\t""\t""\t""\t""\t""\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("data kelembapan")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("\t")+String("data temperatur");
  header1=String("HARI")+String("\t")+String("TANGGAL")+String("\t")+String("PUKUL")+String("\t")+String("Data ke")+String("\t")+String("waktu 1")+String("\t")+String("waktu 2")+String("\t")+String("waktu 3")+String("\t")+String("waktu 4")+String("\t")+String("waktu 5")+String("\t")+String("satuan")+String("\t")+nosensor;
  String a = "=================================================================================================================================================================================";

  Serial.println(a);
  Serial.println(header);
  Serial.println(header1);  
  bunyi(150, 10, 3);
  tampillcd("",">              <"); delay(100);
  tampillcd("",">>            <<"); delay(100);
  tampillcd("",">>>          <<<"); delay(100);
  tampillcd("",">>> INSGREEB <<<"); delay(100);
  tampillcd(">>> INSGREEB <<<",""); delay(2000);
  //delay(500);
  File dataSensor = SD.open("datalog.txt", FILE_WRITE);
  
  if (dataSensor) 
  {
    dataSensor.println(a);
    dataSensor.println(header);
    dataSensor.println(header1);
    tampillcd("DATALOGGER","TERDETEKSI");
    dataSensor.close();
    delay(1000);
  }
  else {
    Serial.println("gagal membuka file .txt");
    dataSensor.println("gagal membuka file .txt");
    tampillcd("DATALOGGER","TDK TERDETEKSI");
    delay(1000);
  }
  
  bacakonfigurasi();
  
  if(statuskalibrasi==0){tampillcd("kalibrasi:", "belum"); delay(1000);}
  if(statuskalibrasi==1){tampillcd("kalibrasi:", "global"); delay(1000);}
  if(statuskalibrasi==2){tampillcd("kalibrasi:", "parsial | "+String(nsensorkalib)); delay(1000);}
  if(statuskalibrasi==3){tampillcd("kalibrasi:", "seluruh | 16"); delay(1000);}
  
}

void loop() 
{
  //Serial.println(String(waktukerja));
  //Serial.println(ind);
  //delay(10);
  switch(ind)
  {
  case 0: ind=0; tampilawal(); break;
  case 1: ind=1; start(); break;
  case 2: ind=2; tampilmenu(); break;
  case 3: ind=3; setwaktu(); break;
  case 4: ind=4; menukalib(); break;
  case 5: ind=5; refdata_h(); break;
  case 6: ind=6; refdata_t(); break;
  case 7: ind=7; kekalib(); break;
  case 8: ind=8; kalibrasi(); break;
  case 9: ind=9; standby(); break;
  case 10: ind=10; selesai(); break;
  case 11: ind=11; setwaktudetik(); break;
  case 12: ind=12; setwaktumenit(); break;
  case 13: ind=13; setwaktujam(); break;
  case 14: ind=14; ndata(); break;
  case 15: ind=15; modekerja(); break;
  case 16: ind=16; preview(); break;
  case 17: ind=17; hapuseeprom(); break;
  case 18: ind=18; periksa(); break;
  case 19: ind=19; previewkoef(); break;
  case 20: ind=20; editkoef_h(); break;
  case 21: ind=21; editkoef_t(); break;
  case 22: ind=22; kalibkoef(); break;
  case 23: ind=23; jumlahnkalib(); break;
  case 24: ind=24; datankalib(); break;
    //default: tampilawal(); break;
  }
}

void start()
{
  String datasensor="";
  n++;
  bunyitombol();
  tampillcd("pengambilan data","ke "+String(n));
  //delay(1000);
  waktu = String(millis());
  //waktuset=EEPROM.read(waktukerja);
  if(millis() > 0)//isiwaktu[waktuset])
  {
    tampillcd("mengambil data :","kelembapan");
    hitungwaktu();
    cekkelembapan=waktuhitung;
    h[0] = dht0.readHumidity()*hrefa1[0]+hrefa0[0];
    h[1] = dht1.readHumidity()*hrefa1[1]+hrefa0[1];
    h[2] = dht2.readHumidity()*hrefa1[2]+hrefa0[2];
    h[3] = dht3.readHumidity()*hrefa1[3]+hrefa0[3];
    h[4] = dht4.readHumidity()*hrefa1[4]+hrefa0[4];
    h[5] = dht5.readHumidity()*hrefa1[5]+hrefa0[5];
    h[6] = dht6.readHumidity()*hrefa1[6]+hrefa0[6];
    h[7] = dht7.readHumidity()*hrefa1[7]+hrefa0[7];
    h[8] = dht8.readHumidity()*hrefa1[8]+hrefa0[8];
    h[9] = dht9.readHumidity()*hrefa1[9]+hrefa0[9];
    h[10] = dht10.readHumidity()*hrefa1[10]+hrefa0[10];
    h[11] = dht11.readHumidity()*hrefa1[11]+hrefa0[11];
    h[12] = dht12.readHumidity()*hrefa1[12]+hrefa0[12];
    h[13] = dht13.readHumidity()*hrefa1[13]+hrefa0[13];
    h[14] = dht14.readHumidity()*hrefa1[14]+hrefa0[14];
    h[15] = dht15.readHumidity()*hrefa1[15]+hrefa0[15];

    hitungwaktu();
    waktuselesaikelembapan = waktuhitung;
    tampillcd(String(h[0])+"||"+String(h[1]),String(h[2])+"||"+String(h[3]));
    delay(300);
    tampillcd(String(h[4])+"||"+String(h[5]),String(h[6])+"||"+String(h[7]));
    delay(300);
    tampillcd(String(h[8])+"||"+String(h[9]),String(h[10])+"||"+String(h[11]));
    delay(300);
    tampillcd(String(h[12])+"||"+String(h[13]),String(h[14])+"||"+String(h[15]));
    delay(300);
    for(int k=0; k<16; k++)
    {
      if(isnan(h[k]))
      {
        datasensor+=String("error");
      }
      else datasensor+=String(h[k],3);        //String(floatToString(desimalmaks, h[k], presisi, lebardesimal))
      if(k<15)
      {
        datasensor+=String("\t");
      }
    }
    tampillcd("Selesai...","");
    bunyitombol();
    datasensor+=String("\t");

    tampillcd("mengambil data :","temperatur");
    hitungwaktu();
    cektemperatur =waktuhitung;
    t[0] = dht0.readTemperature()*trefa1[0]+trefa0[0];  
    t[1] = dht1.readTemperature()*trefa1[1]+trefa0[1];
    t[2] = dht2.readTemperature()*trefa1[2]+trefa0[2];
    t[3] = dht3.readTemperature()*trefa1[3]+trefa0[3];
    t[4] = dht4.readTemperature()*trefa1[4]+trefa0[4];
    t[5] = dht5.readTemperature()*trefa1[5]+trefa0[5];
    t[6] = dht6.readTemperature()*trefa1[6]+trefa0[6];
    t[7] = dht7.readTemperature()*trefa1[7]+trefa0[7];
    t[8] = dht8.readTemperature()*trefa1[8]+trefa0[8];
    t[9] = dht9.readTemperature()*trefa1[9]+trefa0[9];
    t[10] = dht10.readTemperature()*trefa1[10]+trefa0[10];
    t[11] = dht11.readTemperature()*trefa1[11]+trefa0[11];
    t[12] = dht12.readTemperature()*trefa1[12]+trefa0[12];
    t[13] = dht13.readTemperature()*trefa1[13]+trefa0[13];
    t[14] = dht14.readTemperature()*trefa1[14]+trefa0[14];
    t[15] = dht15.readTemperature()*trefa1[15]+trefa0[15];

    hitungwaktu();
    waktuselesaitemperatur = waktuhitung;
    tampillcd(String(t[0])+"||"+String(t[1]),String(t[2])+"||"+String(t[3]));
    delay(300);
    tampillcd(String(t[4])+"||"+String(t[5]),String(t[6])+"||"+String(t[7]));
    delay(300);
    tampillcd(String(t[8])+"||"+String(t[9]),String(t[10])+"||"+String(t[11]));
    delay(300);
    tampillcd(String(t[12])+"||"+String(t[13]),String(t[14])+"||"+String(t[15]));
    delay(300);
    
    for(int j=0; j<16; j++)
    {
      if(isnan(t[j]))
      {
        datasensor+=String("error");
      }
      else datasensor+=String(t[j],3);  //String(floatToString(desimalmaks, t[j], presisi, lebardesimal))

      if(j<15)
      {
        datasensor+=String("\t");
      }

    }
    tampillcd("Selesai...","");
    datasensor=String(rtc.getDOWStr())+String("\t")+String(rtc.getDateStr())+String("\t")+String(rtc.getTimeStr())+String("\t")+String(n)+String("\t")+String(waktu)+String("\t")+String(cekkelembapan)+String("\t")+String(waktuselesaikelembapan)+String("\t")+String(cektemperatur)+String("\t")+String(waktuselesaitemperatur)+String("\t")+String(satuan)+String("\t")+datasensor;
    //Serial.println(datasensor);
    File dataSensor = SD.open("datalog.txt", FILE_WRITE);
    if (dataSensor) 
    {
      dataSensor.println(datasensor);
      dataSensor.close();
       Serial.println(datasensor);
      //tampillcd("Selesai...","");
      //bunyitombol();
      // print to the serial port too:
      //Serial.println(datasensor);
    }  
    // if the file isn't open, pop up an error:
    else {
      Serial.println("gagal membuka file .txt");
      Serial.println(datasensor);
      tampillcd("gagal membuka","file...");
      delay(500);
      //return 0;
    }

    //n_sebelum = n;
  
  }
  if(caraambildata=="langsung"){Serial.println(String(millis()));}
  
  if(caraambildata=="jumlah"){ 
    tampillcd("sisa data :",String(nmaks-n)+" lagi");
    if(n==nmaks){ind=10; selesai();}
    delay(1000);
  }
  
  if(caraambildata=="waktu"){ 
    if(bacawaktujam>0) {tampillcd("menunggu :",String(bacawaktujam)+" jam "+String(bacawaktumenit)+" mnt "+String(bacawaktudetik)+" dtk");}
    if(bacawaktumenit>0) {tampillcd("menunggu :",String(bacawaktumenit)+" menit "+String(bacawaktudetik)+" detik");}
    if(bacawaktumenit==0 && bacawaktujam==0) {tampillcd("menunggu :",String(bacawaktudetik)+" detik");}
    //if(bacawaktujam==0) tampillcd("menunngu :",String(bacawaktumenit)+" menit "+String(bacawaktudetik)+" detik");
    delay(waktukerja);
  }
  if(caraambildata=="waktu dan jumlah"){
    tampillcd("sisa data :",String(nmaks-n)+" lagi");
    if(n==nmaks){ind=10; selesai();}
    delay(1000); 
    if(bacawaktujam>0) {tampillcd("menunggu :",String(bacawaktujam)+" jam "+String(bacawaktumenit)+" mnt "+String(bacawaktudetik)+" dtk");}
    if(bacawaktumenit>0) {tampillcd("menunggu :",String(bacawaktumenit)+" menit "+String(bacawaktudetik)+" detik");}
    if(bacawaktumenit==0 && bacawaktujam==0) {tampillcd("menunggu :",String(bacawaktudetik)+" detik");}
    //if(bacawaktujam==0) tampillcd("menunngu :",String(bacawaktumenit)+" menit "+String(bacawaktudetik)+" detik");
    delay(waktukerja);
  }
  
  tombol();
  if(stateselect==LOW)
  {
    bunyitombol;
    ind=9;
    standby();
  }
  if(statemenu==LOW)
  {
    bunyitombol;
    ind=2;
    tampilmenu();
  }
}

void bacakonfigurasi(){
  
  statuskalibrasi=0;
  nsensorkalib=0;
  koefstatus=0;
  bacawaktudetik=0;
  bacawaktumenit=0;
  bacawaktujam=0;
  waktukerja=0;
  nmaks=0;
  modex=0;
  caraambildata="";
  
  statuskalibrasi=EEPROM.read(statuskalibaddr);
  nsensorkalib=EEPROM.read(nsensorkalibaddr);
  koefstatus=EEPROM.read(koefaddr);
  
  bacawaktudetik=EEPROM.read(waktudetik);
  bacawaktumenit=EEPROM.read(waktumenit);
  bacawaktujam=EEPROM.read(waktujam);
  
  waktukerja=(bacawaktudetik+(bacawaktumenit*60)+(bacawaktujam*3600))*1000;
  
  for(int i=7; i<47; i++)
  {
    nmaks+=EEPROM.read(i);
  }
  
  modex=EEPROM.read(47);
  if(modex==1){caraambildata="langsung";}
  if(modex==2){caraambildata="jumlah";}
  if(modex==3){caraambildata="waktu";}
  if(modex==4){caraambildata="waktu dan jumlah";}
  
  if(statuskalibrasi==0 || koefstatus==0){
    for(int i=0; i<16; i++)
    {
       hrefa1[i]=1; hrefa0[i]=0;
       trefa1[i]=1; trefa0[i]=0;
    }
     statuskalibrasi=0;
  }
  if(statuskalibrasi==1){
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+48, hrefa0[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+112, hrefa1[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+176, trefa0[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+240, trefa1[i]);
      }
  }
  if(statuskalibrasi==2 || statuskalibrasi==3){ 
    if(koefstatus==1){
      for(int i=0; i<16; i++){
        hrefa0[i]=manhrefa0[i];
        hrefa1[i]=manhrefa1[i];
        trefa0[i]=mantrefa0[i];
        trefa1[i]=mantrefa1[i];
      }
    }
    if(koefstatus==2){
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+48, hrefa0[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+112, hrefa1[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+176, trefa0[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+240, trefa1[i]);
      }
    }
  }
    if(koefstatus==1){
      for(int i=0; i<16; i++){
        hrefa0[i]=manhrefa0[i];
        hrefa1[i]=manhrefa1[i];
        trefa0[i]=mantrefa0[i];
        trefa1[i]=mantrefa1[i];
      }
    }
    if(koefstatus==2){
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+48, hrefa0[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+112, hrefa1[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+176, trefa0[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+240, trefa1[i]);
      }
    }

    if(statuskalibrasi==0){
    for(int i=0; i<16; i++)
    {
       hrefa1[i]=1; hrefa0[i]=0;
       trefa1[i]=1; trefa0[i]=0;
    }
     statuskalibrasi=0;
  }
  bunyi(150, 10, 2);
}

void hitungwaktu()
{

  if(millis()>=1000 && millis()<60000)
  {
    waktuhitung=String(millis()/1000); 
    satuan="detik";
  }

  if(millis()>=60000 && millis()<3600000)
  {
    waktuhitung=String(millis()/60000)+String(":")+String((millis()%60000)/1000);
    satuan = "menit";  
  }

  if(millis()>3600000)
  {
    //inString.substring();
    waktuhitung=String(millis()/3600000+String(":"))+String((millis()%3600000)/60000)+String(":")+String(((millis()%3600000)%60000)/1000);
    satuan = "jam";
  }

  //return &waktuhitung;
}

void tampillcd(String kolom1, String kolom2)
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(kolom1);
  lcd.setCursor(0,1);
  lcd.print(kolom2);
}


void bunyitombol()
{
  //tone(24,420,200);
  digitalWrite(buzzer,HIGH);
  delay(100);
  digitalWrite(buzzer,LOW);
}

void bunyi(unsigned long waktubunyi1, unsigned long waktubunyi2, int x )
{
  //tone(24,420,200);
  for(int i=0; i<x; i++){
    digitalWrite(buzzer,HIGH);
    delay(waktubunyi1);
    digitalWrite(buzzer,LOW);
    delay(waktubunyi2);
  }
}


void tampilawal()
{
  tombol();
  tampillcd("OK  ->Mulai","MENU->Pengaturan");
  delay(300);
  //delay(250);
  if(stateselect == LOW || stateup == LOW)
  {
    bunyitombol();
    resettombol();
    tampillcd("membaca","konfigurasi"); 
    bacakonfigurasi(); 
    tampillcd("=== SELESAI ===","");
    delay(1000);  
    ind=1;
    start();   
  }
  if(statemenu==LOW || statedown==LOW)
  {
    bunyitombol();
    resettombol();  
    ind=2;
    indmenu=1;
    tampilmenu();
  }
}

void tampilmenu()
{
  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    indmenu++;
    //if(indmenu==10){ind=0; indmenu=10; tampilawal();}
    if(indmenu>10)
    {
      indmenu=1;
    }
  }
  
  if(statedown==LOW)
  {
    bunyitombol();
    indmenu--;
    //if(indmenu==10){ind=0; indmenu=10; tampilawal();}
    if(indmenu<1)
    {
      indmenu=10;
    }
  }

  if(indmenu==1)
  {
    tampillcd("##### MENU #####", "OK -> ambil data");
  }
  if(indmenu==2)
  {
    tampillcd("##### MENU #####", "OK -> set waktu");
  }
  if(indmenu==3)
  {
    tampillcd("##### MENU #####", "OK -> set N data");
  }
  if(indmenu==4)
  {
    tampillcd("##### MENU #####", "OK -> set mode");
  }
  if(indmenu==5)
  {
    tampillcd("##### MENU #####", "OK -> kalibrasi");
  }
  if(indmenu==6)
  {
    tampillcd("##### MENU #####", "OK -> preview");
  }
  if(indmenu==7)
  {
    tampillcd("##### MENU #####", "OK -> Hapus Data");
  }
  if(indmenu==8)
  {
    tampillcd("##### MENU #####", "OK -> periksa");
  }
  if(indmenu==9)
  {
    tampillcd("##### MENU #####", "OK -> edit koef.");
  }
  if(indmenu==10)
  {
    tampillcd("##### MENU #####", "OK -> koef kalib");
  }
  delay(195);                  //-------------------------------------------> penting
  if(stateselect==LOW)
  {
    bunyitombol();
    switch(indmenu)
    {
      //case 0: tampilmenu(); break;
    case 1: 
      indmenu=0;
      bunyitombol();
      resettombol();
      tampillcd("membaca","konfigurasi"); 
      bacakonfigurasi(); 
      tampillcd("=== SELESAI ===","");
      ind=1; 
      delay(1000);  
      start(); 
      break;
    case 2: 
      indmenu=0;
      nsetwaktu=1; 
      ind=3;
      setwaktu(); 
      break;
    case 3: 
      indmenu=0;
      resettombol();
      //jumlahdata=1; 
      ind=14;
      //ndata(); 
      delay(500);
      break;
    case 4: 
      indmenu=0;
      resettombol();
      mode=1;
      ind=15; 
      //modekerja(); 
      delay(500);
      break;
    case 5: 
      ind=4; 
      indmenu=0;
      indmenukalib=1;
      tampillcd("masukkan 5 data", "sensor valid"); 
      delay(500);
      break;
    case 6: 
      ind=16; 
      indmenu=0;
      npreview=1;
      resettombol();
      delay(500); 
      break;
   case 7: 
      ind=17; 
      indmenu=0;
      neeprom=1;
      resettombol();
      delay(500); 
      break;
    case 8: 
      ind=18; 
      indmenu=0;
      nperiksa=1;
      resettombol();
      tampillcd("MEMERIKSA","");
      xbacasensor(0);
      for(int i=0; i<16; i++)
      {
        if(isnan(xh[0][i]) && isnan(xt[0][i])){
          statusnsensor[i]="TDK TERDETEKSI";
        }
        if(!isnan(xh[0][i]) || !isnan(xt[0][i])){
          statusnsensor[i]="TERDETEKSI";
        }
        
        if(isnan(xh[0][i]) && !isnan(xt[0][i])){
          statusnsensor[i]="KELEMBAPAN ERROR";
        }
        if(!isnan(xh[0][i]) && isnan(xt[0][i])){
          statusnsensor[i]="TEMPERATUR ERROR";
        }
        
      }
      
      delay(300);
      if (!SD.begin(chipSelect)) {
          statuslogger="TDK TERDETEKSI";
          //return;
      }
      else{
          statuslogger="TERDETEKSI";
          //return;
      }
      tampillcd("MEMERIKSA","SELESAI");
      bunyi(150, 50, 2);
      delay(300);
      
      /*
      File tesmicrosd = SD.open("periksamikrosd.txt", FILE_WRITE);
      if(tesmicrosd){
        statuslogger="microSD OK";
      }
      else{
        statuslogger="microSD ERROR";
      }
      */
      delay(500); 
      break;
    case 9: 
      ind=20; 
      indmenu=0;
      datake=0;
      nedit=1;
      resettombol();
      delay(500); 
      break;
    case 10: 
      ind=22; 
      indmenu=0;
      koefstatus;
      resettombol();
      delay(500);
      break;
    }

  }
}

void tombol()
{
  stateup = digitalRead(up);
  statedown = digitalRead(down);
  stateselect = digitalRead(select);
  statemenu = digitalRead(menu);
  delay(40);
  //tone(A0 , 400, 200);
} 

void resettombol(){
  stateup=0;
  statedown=0;
  statemenu=0;
  stateselect=0;
}

void setwaktu() // ind = 3
{
  bacawaktudetik=EEPROM.read(waktudetik);
  bacawaktumenit=EEPROM.read(waktumenit);
  bacawaktujam=EEPROM.read(waktujam);
  tampillcd("jam menit detik",String(bacawaktujam)+"   "+String(bacawaktumenit)+"   "+String(bacawaktudetik));
  delay(250);
  tombol();
  //delay(300);
  if(stateup==LOW)
  {
    bunyitombol();
    nsetwaktu++;
    if(nsetwaktu>3) nsetwaktu=1;
    
  }
  if(statedown==LOW)
  {
    bunyitombol();
    nsetwaktu--;
    if(nsetwaktu<1) nsetwaktu=3;
  }
  
  if(nsetwaktu==1){tampillcd("set waktu :","detik");}
  if(nsetwaktu==2){tampillcd("set waktu :","menit>detik");}
  if(nsetwaktu==3){tampillcd("set waktu :","jam>menit>detik");}
  delay(250);
  if(stateselect==LOW)
  {
    if(nsetwaktu==1){
      ind=11;
      //indmenu=10;
      setwaktudetik();
      //delay(300);
    }
    if(nsetwaktu==2){
      ind=12;
      //indmenu=10;
      setwaktumenit();
      //delay(300);
    }
    if(nsetwaktu==3){
      ind=13;
      //indmenu=10;
      setwaktujam();
      //delay(300);
    }
  }
}

void setwaktudetik() // ind = 11
{
  //int a;
  
  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    nsetwaktudetik++;
    if(nsetwaktudetik>59) nsetwaktudetik=0;
    
  }
  if(statedown==LOW)
  {
    bunyitombol();
    nsetwaktudetik--;
    if(nsetwaktudetik<0) nsetwaktudetik=59;
  }
  tampillcd("set waktu(detik)",String(nsetwaktudetik));
  delay(250);
  if(stateselect==LOW)
  {
    bunyitombol();
    EEPROM.write(waktudetik, 0);
    EEPROM.write(waktudetik, nsetwaktudetik);
    tampillcd("set waktu data","selesai...");
    bacawaktudetik=EEPROM.read(waktudetik);
    bacawaktumenit=EEPROM.read(waktumenit);
    bacawaktujam=EEPROM.read(waktujam);
    waktukerja=(bacawaktudetik+(bacawaktumenit*60)+(bacawaktujam*3600))*1000;
    delay(1000);
    ind=2;
    indmenu=1;
    tampilmenu();
  }
}

void setwaktumenit() // ind = 12
{
  //int a;
  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    nsetwaktumenit++;
    if(nsetwaktumenit>59) nsetwaktumenit=0;
    
  }
  if(statedown==LOW)
  {
    bunyitombol();
    nsetwaktumenit--;
    if(nsetwaktumenit<0) nsetwaktumenit=59;
  }
  tampillcd("set waktu(menit)",String(nsetwaktumenit));
  delay(250);
  if(stateselect==LOW)
  {
    bunyitombol();
    EEPROM.write(waktumenit, 0);
    EEPROM.write(waktumenit, nsetwaktumenit);
    delay(1000);
    ind=11;
    setwaktudetik();
    //delay(300);
  }
}

void setwaktujam() // ind = 12
{
  //int a;
  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    nsetwaktujam++;
    if(nsetwaktujam>255) nsetwaktujam=0;
    
  }
  if(statedown==LOW)
  {
    bunyitombol();
    nsetwaktujam--;
    if(nsetwaktujam<0) nsetwaktujam=255;
  }
  tampillcd("set waktu(jam)",String(nsetwaktujam));
  delay(250);
  if(stateselect==LOW)
  {
    bunyitombol();
    EEPROM.write(waktujam, 0);
    EEPROM.write(waktujam, nsetwaktujam);
    delay(1000);
    ind=12;
    //indmenu=10;
    setwaktumenit();
    //delay(300);
  }
}

void menukalib()  //ind = 4
{
  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    indmenukalib++;
    if(indmenukalib>2){indmenukalib=1;}
  }
  if(statedown==LOW)
  {
    bunyitombol();
    indmenukalib--;
    if(indmenukalib<1){indmenukalib=2;}
  }
  if(indmenukalib==1){tampillcd(" TIPE KALIBRASI ", "> GLOBAL");}
  if(indmenukalib==2){tampillcd(" TIPE KALIBRASI ", "> PER SENSOR ");}
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    statuskalibrasi=indmenukalib; tampillcd("masukkan jumlah", "data kalibrasi"); delay(1000);
    nkalib=1;
    resettombol();
    delay(500);
    ind=24; 
    datankalib();
  }
  if(statemenu==LOW)
  {
    bunyitombol();
    ind=2;
    tampilmenu();
  }
}

void datankalib(){
  
  tombol();
  //delay(100);
  if(stateup==LOW)
  {
    bunyitombol();
    ndatakalib++;
    if(ndatakalib>3){ndatakalib=1;}
  }
  if(statedown==LOW)
  {
    bunyitombol();
    ndatakalib--;
    if(ndatakalib<1){ndatakalib=3;}
  }
  tampillcd("Jumlah Data:", String(ndatakalib));
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    //ind=5; statuskalibrasi=indmenukalib; tampillcd("masukkan jumlah", "sensor kalibrasi"); delay(1000);
    ind=23; ndatakalibrasi=ndatakalib; tampillcd("masukkan jumlah", "sensor kalibrasi"); delay(1000); jumlahnkalib();
  }
  if(statemenu==LOW)
  {
    bunyitombol();
    ind=2;
    tampilmenu();
  }
}

void jumlahnkalib()
{
  tombol();
  delay(300);
  if(stateup==LOW)
  {
    bunyitombol();
    nkalib++;
    if(nkalib>16){nkalib=1;}
  }
  if(statedown==LOW)
  {
    bunyitombol();
    nkalib--;
    if(nkalib<1){nkalib=16;}
  }
  tampillcd("JUMLAH SENSOR ", String(nkalib));
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    if(statuskalibrasi==1){ind=5; statuskalibrasi=1; nsensorkalib=nkalib; sensorke=0; datake=0; komm=0; delay(500);}
    if(statuskalibrasi==2 && nkalib<16){ind=5; statuskalibrasi=2; nsensorkalib=nkalib; sensorke=0; datake=0; komm=0; delay(500);}
    if(statuskalibrasi==2 && nkalib==16){ind=5; statuskalibrasi=3; nsensorkalib=nkalib; sensorke=0; datake=0; komm=0; delay(500);}
  }
  if(statemenu==LOW)
  {
    bunyitombol();
    ind=2;
    tampilmenu();
  }
}

void refdata_h()  //ind = 5
{
  //int datake;
  //lcd.setCursor(0,0);
  tampillcd("H ke: "+String(sensorke+1), "data: "+String(datake+1)+" : "+String(datakelembapan[datake][sensorke]));
  datakeypad();
  datakelembapan[datake][sensorke]=ang;//=datakeypad();
  tombol();
  delay(300);
  if(stateup==LOW || statedown==LOW)
  {
    bunyitombol();
    ang=-1*ang;
  }
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    resettombol();
    ind=6;
    delay(500);
    //refdata_t();
  }
  
  if(statemenu==LOW)
  {
    bunyitombol();
    ind=2;
    tampilmenu();
  }
} 

void refdata_t()  //ind = 6
{
  //lcd.setCursor(0,0);
  tampillcd("T ke: "+String(sensorke+1), "data: "+String(datake+1)+" : "+String(datatemperatur[datake][sensorke]));
  datakeypad();
  datatemperatur[datake][sensorke]=ang;//=datakeypad();
  tombol();
  delay(300);
  if(stateup==LOW || statedown==LOW)
  {
    bunyitombol();
    ang=-1*ang;
  }
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    
    if(sensorke<=(nsensorkalib-1)){
      if(datake<(ndatakalibrasi-1)){datake++; resettombol(); ind=5; refdata_h(); delay(500);}
      if(datake>(ndatakalibrasi-1)){  
        sensorke++;
        datake=0;
        //datake++;
        resettombol();
        delay(500);
        ind=5;
        //refdata_h();
        
     }
    }
     if(sensorke>(nsensorkalib-1)){
        //if(datake<ndatakalibrasi){ind=5; resettombol(); delay(500);}
         ndataref=datake;
         datake=0;
         sensorke=0;
         tampillcd("","");
         delay(300);
         tampillcd("tempatkan DHT","ditempat yang");
         delay(350);
         tampillcd("sama seperti","referensi");
         resettombol();
         ind=7;
         kekalib();
         delay(500);
         //break;
         
        }
        //kekalib();
        //resettombol();
        
  
 
    //refdata_t();
  }
  if(statemenu==LOW)
  {
    bunyitombol();
    ind=2;
    tampilmenu();
  }
}

void datakeypad()
{
   char key = pad.getKey();
      switch (key)
      {
       case NO_KEY: break;
       case '.': komm=1; bunyitombol(); break;
       case '0': case '1': case '2': case '3': case '4': case '5': 
       case '6': case '7': case '8': case '9':
         bunyitombol();
         if(komm==0)
          {
            ang = ang * 10 + (key - '0');
            //datatemperatur[datake] = datatemperatur[datake] * 10 + (key - '0');
          }
            
          if(komm>0)
          {
            ang = ang + (pow(0.1,komm))*(key - '0');
            komm++;
            //delay(150);
          }
          break;
        case'#': bunyitombol(); ang=0; komm=0; break;
      }
}

void kekalib()  //ind =7
{
  //float d,e;
  tampillcd("sudah?","OK -> lanjut");
  tombol();
  delay(300);  
  if(stateselect==LOW)
  {
    bunyitombol();
    ind=8;
    delay(500);
  }
}
/*
void bacasensor(){
    h[0] = dht0.readHumidity();
    h[1] = dht1.readHumidity();
    h[2] = dht2.readHumidity();
    h[3] = dht3.readHumidity();
    h[4] = dht4.readHumidity();
    h[5] = dht5.readHumidity();
    h[6] = dht6.readHumidity();
    h[7] = dht7.readHumidity();
    h[8] = dht8.readHumidity();
    h[9] = dht9.readHumidity();
    h[10] = dht10.readHumidity();
    h[11] = dht11.readHumidity();
    h[12] = dht12.readHumidity();
    h[13] = dht13.readHumidity();
    h[14] = dht14.readHumidity();
    h[15] = dht15.readHumidity();
    
    t[0] = dht0.readTemperature();  
    t[1] = dht1.readTemperature();
    t[2] = dht2.readTemperature();
    t[3] = dht3.readTemperature();
    t[4] = dht4.readTemperature();
    t[5] = dht5.readTemperature();  
    t[6] = dht6.readTemperature();
    t[7] = dht7.readTemperature();
    t[8] = dht8.readTemperature();
    t[9] = dht9.readTemperature();
    t[10] = dht10.readTemperature();  
    t[11] = dht11.readTemperature();
    t[12] = dht12.readTemperature();
    t[13] = dht13.readTemperature();
    t[14] = dht14.readTemperature();
    t[15] = dht15.readTemperature();
}
*/
void xbacasensor(int i)
{
    xh[i][0] = dht0.readHumidity();
    xh[i][1] = dht1.readHumidity();
    xh[i][2] = dht2.readHumidity();
    xh[i][3] = dht3.readHumidity();
    xh[i][4] = dht4.readHumidity();
    xh[i][5] = dht5.readHumidity();
    xh[i][6] = dht6.readHumidity();
    xh[i][7] = dht7.readHumidity();
    xh[i][8] = dht8.readHumidity();
    xh[i][9] = dht9.readHumidity();
    xh[i][10] = dht10.readHumidity();
    xh[i][11] = dht11.readHumidity();
    xh[i][12] = dht12.readHumidity();
    xh[i][13] = dht13.readHumidity();
    xh[i][14] = dht14.readHumidity();
    xh[i][15] = dht15.readHumidity();
    
    xt[i][0] = dht0.readTemperature();  
    xt[i][1] = dht1.readTemperature();
    xt[i][2] = dht2.readTemperature();
    xt[i][3] = dht3.readTemperature();
    xt[i][4] = dht4.readTemperature();
    xt[i][5] = dht5.readTemperature();  
    xt[i][6] = dht6.readTemperature();
    xt[i][7] = dht7.readTemperature();
    xt[i][8] = dht8.readTemperature();
    xt[i][9] = dht9.readTemperature();
    xt[i][10] = dht10.readTemperature();  
    xt[i][11] = dht11.readTemperature();
    xt[i][12] = dht12.readTemperature();
    xt[i][13] = dht13.readTemperature();
    xt[i][14] = dht14.readTemperature();
    xt[i][15] = dht15.readTemperature();
}

void kalibrasi()  // ind = 8
{
    tampillcd("kalibrasi...","");
    for(int i=0; i<nrefdata; i++)
    {
      xbacasensor(i);
    }
   
    //hrefdata[i][j]=datakelembapan[i][j];            //-------perbaiki
      //  trefdata[i][j]=datatemperatur[i][j];
    for(int j=1; j<=nsensorkalib; j++)
    {
      for(int i=1; i<=ndataref; i++){
        hrefdata[i][j]=datakelembapan[i][j];            //-------perbaiki
        trefdata[i][j]=datatemperatur[i][j];
        hsigmaxiyi=hsigmaxiyi+(hrefdata[i][j]*xh[i][j]);
        hsigmaxi=hsigmaxi+xh[i][j];    
        hsigmayi=hsigmayi+hrefdata[i][j];
        hsigmaxiquad=hsigmaxiquad+(xh[i][j]*xh[i][j]);
        tsigmaxiyi=tsigmaxiyi+(trefdata[i][j]*xt[i][j]);
        tsigmaxi=tsigmaxi+xt[i][j];
        tsigmayi=tsigmayi+trefdata[i][j];
        tsigmaxiquad=tsigmaxiquad+(xt[i][j]*xt[i][j]);
      }
      xha1[j]=(jumdata*hsigmaxiyi-hsigmaxi*hsigmayi)/(jumdata*hsigmaxiquad-(hsigmaxi*hsigmaxi));      //-----perbaiki
      xta1[j]=(jumdata*tsigmaxiyi-tsigmaxi*tsigmayi)/(jumdata*tsigmaxiquad-(tsigmaxi*tsigmaxi));
      xha0[j]=(hsigmaxiquad*hsigmayi-hsigmaxi*hsigmaxiyi)/(jumdata*hsigmaxiquad-(hsigmaxi*hsigmaxi));
      xta0[j]=(tsigmaxiquad*tsigmayi-tsigmaxi*tsigmaxiyi)/(jumdata*tsigmaxiquad-(tsigmaxi*tsigmaxi));
    }
    
    for(int i=0; i<16; i++){
      ha0[i]=0; ha1[i]=0; ta1[i]=0; ta0[i]=0;
    }
    
    if(statuskalibrasi==1){
      for(int i=0; i<nsensorkalib; i++){
        nxha0= nxha1+xha0[i];
        nxha1= nxha1+xha1[i];
        nxta0= nxha1+xta0[i];
        nxta1= nxha1+xta1[i];
      }
      nxha0=nxha0/nsensorkalib;
      nxha1=nxha1/nsensorkalib;
      nxta0=nxta0/nsensorkalib;
      nxta1=nxta1/nsensorkalib;
      for(int i=0; i<16; i++){
        ha0[i]=nxha0; ha1[i]=nxha1; ta1[i]=nxta1; ta0[i]=nxta0;
      }
    }
    
    if(statuskalibrasi==2 || statuskalibrasi==3){
      for(int i=0; i<nsensorkalib; i++){
        ha0[i]=xha0[i]; ha1[i]=xha1[i]; ta1[i]=xta1[i]; ta0[i]=xta0[i];
      }
    }
    if(nsensorkalib<16){statuskalibrasi==2;}
    if(nsensorkalib==16){statuskalibrasi==3;}
    
    for(int i=0; i<16; i++)
    {
      tuliseeprom((i*4)+48, ha0[i]);
    }
    for(int i=0; i<16; i++)
    {
      tuliseeprom((i*4)+112, ha1[i]);
    }
    for(int i=0; i<16; i++)
    {
      tuliseeprom((i*4)+176, ta0[i]);
    }
    for(int i=0; i<16; i++)
    {
      tuliseeprom((i*4)+240, ta1[i]);
    }
       
    tampillcd("kalibrasi","selesai");
    delay(100);
    tampillcd("kalibrasi","selesai.");
    delay(100);
    tampillcd("kalibrasi","selesai..");
    delay(100);
    tampillcd("kalibrasi","selesai... :)");
    statuskalibrasi=1;
    EEPROM.write(0,0);
    EEPROM.write(0,statuskalibrasi);
    
    for(int i=0; i<16; i++)
    {
      bacaeeprom((i*4)+48, hrefa0[i]);
    }
    for(int i=0; i<16; i++)
    {
      bacaeeprom((i*4)+112, hrefa1[i]);
    }
    for(int i=0; i<16; i++)
    {
      bacaeeprom((i*4)+176, trefa0[i]);
    }
    for(int i=0; i<16; i++)
    {
      bacaeeprom((i*4)+240, trefa1[i]);
    }
    statuskalibrasi=EEPROM.read(0);
    delay(500);
    ind=2;
    indmenu=0;
    tampilmenu();
}


void standby()
{
  tampillcd("sistem standby","no data record");
  tombol();
  if(stateselect==LOW)
  {
    bunyitombol;
    ind=1;
    start();
  }
  if(statemenu==LOW)
  {
    bunyitombol;
    ind=2;
    tampilmenu();
  }
}

void selesai()
{
  tampillcd("pengambilan data","====selesai====");
  digitalWrite(9,LOW);
  tombol();
  delay(300);
  if(stateup==LOW || statedown==LOW || stateselect==LOW || statemenu==LOW)
  {
    ind=0;
    tampilawal();
    delay(100); 
  }
   
}

void ndata()                                          //ind = 14
{
  tombol();
   
  if(stateup==LOW)
  {
    bunyitombol();
    jumlahdata+=50;
    if(jumlahdata>10200) jumlahdata=0;
  }
  
  if(statedown==LOW)
  {
    bunyitombol();
    jumlahdata-=50;
    if(jumlahdata<0) jumlahdata=10200;
  }
  delay(300);
  tampillcd("N data baru",String (jumlahdata));
  if(stateselect==LOW)
  {
    bunyitombol();
    for(int i=7; i<47; i++)                        //hapus EEPROM dialamat 7-46
    {  
      EEPROM.write(i, 0);
    }
    m=jumlahdata/256;
    if(jumlahdata<256)
    {
      EEPROM.write(7, jumlahdata);
    }
    if((jumlahdata%256)==0)
    {
      for(int i=7; i<7+m; i++)
      {
        EEPROM.write(i, 255);                          //tulis EEPROM dari alamat 7 smp n+7  
      }
    }
    if((jumlahdata>256) && (jumlahdata%256)>0)
    {
      for(int i=7; i<7+m; i++)
      {
        EEPROM.write(i, 255);                          //tulis EEPROM dari alamat 7 smp n+7  
      }
      EEPROM.write(7+m+1, (jumlahdata%256));
    }
                                                      //if((jumlahdata%256)>0){EEPROM.write(7+m+1, (jumlahdata%256));}
    
    tampillcd("-- Set N Data --","selesai...");
    for(int i=7; i<47; i++)
    {
      nmaks+=EEPROM.read(i);
    }
    delay(1000);
    ind=2;
    indmenu=1;
    tampilmenu();
  }
}

void modekerja()
{
  
  
  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    mode++;
    if(mode>4) mode=1;
    
  }
  
  if(statedown==LOW)
  {
    bunyitombol();
    mode--;
    if(mode<1) mode=4;
  }
  if(mode==1){tampillcd("##### MODE #####","> langsung");}
  if(mode==2){tampillcd("##### MODE #####","> N data");}
  if(mode==3){tampillcd("##### MODE #####","> waktu");}
  if(mode==4){tampillcd("##### MODE #####","> waktu & N data");}
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    EEPROM.write(47,0);            //hapus memori EEPROM di alamat 47
    
    EEPROM.write(47, mode);
    
    tampillcd("-- Set Mode --","selesai...");
 
  if(mode==1){caraambildata="langsung";}
  if(mode==2){caraambildata="jumlah";}
  if(mode==3){caraambildata="waktu";}
  if(mode==4){caraambildata="waktu dan jumlah";}
    delay(1000);
    mode=0;
    ind=2;
    indmenu=1;
    tampilmenu();
  }
}

void preview() //ind =16
{
  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    npreview++;
    if(npreview>4) npreview=1;
    
  }
  if(statedown==LOW)
  {
    bunyitombol();
    npreview--;
    if(npreview<1) npreview=4;
  }
  
  if(npreview==1){tampillcd("jam menit detik",String(bacawaktujam)+"   "+String(bacawaktumenit)+"     "+String(bacawaktudetik));}
  if(npreview==2){tampillcd("jumlah data yg","diambil: "+String(nmaks));}
  if(npreview==3){tampillcd("mode kerja:",caraambildata);}
  if(npreview==4){tampillcd("koef. kalibrasi","per sensor");}
  
  //String(floatToString(desimalmaks, hrefa0[], presisi, lebardesimal) String(floatToString(desimalmaks, hrefa1[], presisi, lebardesimal)) String(floatToString(desimalmaks, trefa0[], presisi, lebardesimal))  String(floatToString(desimalmaks, trefa1[], presisi, lebardesimal))
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    if(npreview==4)
    {
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+48, hrefa0[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+112, hrefa1[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+176, trefa0[i]);
      }
      for(int i=0; i<16; i++)
      {
        bacaeeprom((i*4)+240, trefa1[i]);
      }
      ind=19;
      nkoef=1;
      delay(500);
    }
    if(npreview!=4){
    tampillcd("Preview Selesai","selesai...");
    delay(1000);
    ind=2;
    indmenu=1;
    tampilmenu(); 
    }
  }
  if(statemenu==LOW)
  {
    bunyitombol();
    tampillcd("Preview Selesai","selesai...");
    delay(1000);
    ind=2;
    indmenu=1;
    tampilmenu();
  }
}

void previewkoef(){

  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    nkoef++;
    if(nkoef>16) nkoef=1;
    
  }
  
  if(statedown==LOW)
  {
    bunyitombol();
    nkoef--;
    if(nkoef<1) nkoef=16;
  }
  
  //if(nkoef==0){tampillcd("H"+String(nkoef-1)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef-1)+": A0: "+String(hrefa0[nkoef-1]),"A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==1){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==2){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==3){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==4){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==5){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==6){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==7){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==8){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==9){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"    A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==10){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==11){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==12){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==13){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==14){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==15){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1]));delay(500);}
  if(nkoef==16){tampillcd("H"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1])); delay(500); tampillcd("T"+String(nkoef)+": A0: "+String(hrefa0[nkoef-1]),"     A1: "+String(hrefa0[nkoef-1]));delay(500);}
  //delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    ind=16;
    resettombol();
    delay(500);
  }
    
    
}
void hapuseeprom()          //ind=17
{
  tombol();
  
  if(stateup==LOW)
  {
    bunyitombol();
    neeprom++;
    if(neeprom>5) neeprom=1;
    
  }
  
  if(statedown==LOW)
  {
    bunyitombol();
    neeprom--;
    if(neeprom<1) neeprom=5;
  }
  
  if(neeprom==1){tampillcd("--HAPUS EEPROM--","Koef. Kalibrasi");}
  if(neeprom==2){tampillcd("--HAPUS EEPROM--","Set Waktu");}
  if(neeprom==3){tampillcd("--HAPUS EEPROM--","N Data");}
  if(neeprom==4){tampillcd("--HAPUS EEPROM--","Mode Kerja");}
  if(neeprom==5){tampillcd("--HAPUS EEPROM--","Status Koef.");}
  
  //String(floatToString(desimalmaks, hrefa0[], presisi, lebardesimal) String(floatToString(desimalmaks, hrefa1[], presisi, lebardesimal)) String(floatToString(desimalmaks, trefa0[], presisi, lebardesimal))  String(floatToString(desimalmaks, trefa1[], presisi, lebardesimal))
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    if(neeprom==1)
    {
      for(int i=48; i<304; i++)                        //hapus EEPROM dialamat 7-26
      {  
        EEPROM.write(i, 0);
      }
      tampillcd("Koef. Kalibrasi", ">>> TERHAPUS <<<");
      
      EEPROM.write(0,0);
      for(int i=0; i<16; i++)
      {
        hrefa0[i]=0;
        tuliseeprom((i*4)+48, ha0[i]);
      }
      for(int i=0; i<16; i++)
      {
        hrefa1[i]=1;
        tuliseeprom((i*4)+112, ha1[i]);
      }
      for(int i=0; i<16; i++)
      {
        trefa0[i]=0;
        tuliseeprom((i*4)+176, ta0[i]);
      }
      for(int i=0; i<16; i++)
      {
        trefa1[i]=1;
        tuliseeprom((i*4)+240, ta1[i]);
      }
      statuskalibrasi=EEPROM.read(0);
    }
    if(neeprom==2)
    {
      for(int i=4; i<7; i++)                        //hapus EEPROM dialamat 7-26
      {  
        EEPROM.write(i, 0);
      }
      tampillcd("Set Waktu", ">>> TERHAPUS <<<");
      waktukerja=0;
      bacawaktudetik=EEPROM.read(waktudetik);
      bacawaktumenit=EEPROM.read(waktumenit);
      bacawaktujam=EEPROM.read(waktujam);
      waktukerja=(bacawaktudetik+(bacawaktumenit*60)+(bacawaktujam*3600))*1000;
    }
    if(neeprom==3)
    {
      for(int i=7; i<47; i++)                        //hapus EEPROM dialamat 7-26
      {  
        EEPROM.write(i, 0);
      }
      tampillcd("N Data", ">>> TERHAPUS <<<");
      nmaks=0;
      for(int i=7; i<47; i++)                        //hapus EEPROM dialamat 7-26
      {  
        nmaks+=EEPROM.read(i);
      }
    }
    if(neeprom==4){EEPROM.write(47, 0);tampillcd("Mode Kerja", ">>> TERHAPUS <<<");modex=EEPROM.read(47); caraambildata="langsung";}
    if(neeprom==5){EEPROM.write(koefaddr, 0); EEPROM.write(statuskalibaddr, 0); tampillcd("Status Koef.", ">>> TERHAPUS <<<"); koefstatus=EEPROM.read(koefaddr); statuskalibrasi=EEPROM.read(statuskalibaddr);}
    bunyi(150, 50, 2);
    delay(1000);
    ind=2;
    indmenu=1;
    tampilmenu();
  }
  //tampillcd("","");
}

void periksa()        //ind=18
{
  //int nperiksa;
  tombol();
  delay(150);
  
  if(stateup==LOW)
  {
    bunyitombol();
    nperiksa++;
    if(nperiksa>17) nperiksa=1;
    
  }
  
  if(statedown==LOW)
  {
    bunyitombol();
    nperiksa--;
    if(nperiksa<1) nperiksa=17;
  }

  
  
  if(nperiksa==1){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==2){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==3){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==4){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==5){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==6){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==7){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==8){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==9){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==10){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==11){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==12){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==13){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==14){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==15){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==16){tampillcd("Port Sensor: "+String(nperiksa),statusnsensor[nperiksa-1]);}
  if(nperiksa==17){tampillcd("Data Logger:",statuslogger);}
  
  
  //String(floatToString(desimalmaks, hrefa0[], presisi, lebardesimal) String(floatToString(desimalmaks, hrefa1[], presisi, lebardesimal)) String(floatToString(desimalmaks, trefa0[], presisi, lebardesimal))  String(floatToString(desimalmaks, trefa1[], presisi, lebardesimal))
  delay(300);
  if(stateselect==LOW || statemenu==LOW)
  {
    bunyitombol();
    tampillcd("periksa","selesai...");
    delay(1000);
    ind=2;
    indmenu=1;
    tampilmenu();
  }
  //tampillcd("","");
}

void editkoef_h()  //ind = 20
{
  //int datake;
  //lcd.setCursor(0,0);
  //tampillcd("kelembapan ke: "+String(datake+1), String(datakelembapan[datake]));
  tampillcd("H "+String(datake+1)+": A0: "+String(ehrefa0[datake]),"     A1: "+String(ehrefa1[datake]));
  datakeypad();
  //delay(275);
  if(nedit==1){ehrefa0[datake]=ang;}//=datakeypad();
  if(nedit==2){ehrefa1[datake]=ang;}
  tombol();
  delay(300);
  if(stateup==LOW)
  {
    bunyitombol();
    nedit++;
    if(nedit>4){nedit=3;}
  }
  if(statedown==LOW)
  {
    bunyitombol();
    ang=-1*ang;
  }
  //delay(275);
  if(stateselect==LOW)
  {
    bunyitombol();
    nedit++;
    resettombol();
    if(nedit<=2){ind=20; resettombol(); delay(500);}
    if(nedit>2){ind=21; tuliseeprom((datake*4)+48, ehrefa0[datake]); tuliseeprom((datake*4)+112, ehrefa1[datake]); resettombol(); delay(500);}
  }
  
  if(statemenu==LOW)
  {
    bunyitombol();
    ind=2;
    tampillcd("edit koefisien", "dibatalkan");
    delay(1000);
    tampilmenu();
  }
} 

void editkoef_t()  //ind = 21
{
  //lcd.setCursor(0,0);
  tampillcd("T "+String(datake+1)+": A0: "+String(etrefa0[datake]),"     A1: "+String(etrefa1[datake]));
  datakeypad();
  if(nedit==3){etrefa0[datake]=ang;}//=datakeypad();
  if(nedit==4){etrefa1[datake]=ang;}
  tombol();
  delay(300);
  if(stateup==LOW)
  {
    bunyitombol();
    nedit++;
    if(nedit>4){nedit=3;}
  }
  if(statedown==LOW)
  {
    bunyitombol();
    ang=-1*ang;
  }
  //delay(275);
  if(stateselect==LOW)
  {
    bunyitombol();
    datake++;
    nedit++;
    //if(nedit==4){ind=20; resettombol(); delay(500);}
    //if(){ind=21; resettombol(); delay(500);}
    if(datake<15 && nedit<=4){ind=21; tuliseeprom((datake*4)+176, etrefa0[datake]); tuliseeprom((datake*4)+240, etrefa1[datake]); resettombol(); delay(500);}
    if(datake<15 && nedit>4) {ind=20; nedit=1; resettombol(); delay(500);}
    if(datake>15 && nedit>4){
      ind=2; 
      resettombol();
      EEPROM.write(0,1);
      statuskalibrasi=1;
      tampillcd("Edit koef.","Selesai");
      delay(350);
     }//kekalib();
    
    //refdata_t();
  }
  if(statemenu==LOW)
  {
    bunyitombol();
    ind=2;
    tampillcd("edit koefisien", "dibatalkan");
    delay(1000);
    tampilmenu();
  }
}

void kalibkoef()
{
  tombol();
  if(stateup==LOW)
  {
    bunyitombol();
    koefstatus++;
    if(koefstatus>2) {koefstatus=0;} 
  }
  if(statedown==LOW)
  {
    bunyitombol();
    koefstatus--;
    if(koefstatus<0) {koefstatus=2;}
  }
  if(koefstatus==0)tampillcd("-- SET Koef.  --","tidak ada");
  if(koefstatus==1)tampillcd("-- SET Koef.  --","dari Flash");
  if(koefstatus==2)tampillcd("-- SET Koef.  --","dari EEPROM");
  delay(300);
  if(stateselect==LOW)
  {
    bunyitombol();
    EEPROM.write(koefaddr, 0);
    EEPROM.write(koefaddr, koefstatus);
    tampillcd("pengaturan", "koef. selesai");
    koefstatus=EEPROM.read(koefaddr);
    ind=2;
    indmenu=1;
    tampilmenu();
    delay(1000);
  }
}


